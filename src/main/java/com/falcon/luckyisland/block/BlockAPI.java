package com.falcon.luckyisland.block;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public abstract class BlockAPI {

    private static List<Location> placedBlocks;
    private static List<BlockData> destroyedBlocks;
    private static List<Location> remove;

    public static void init() {
        placedBlocks=new ArrayList<>();
        destroyedBlocks=new ArrayList<>();
        remove=new ArrayList<>();
    }

    public static void regen() {
        for (BlockData blockData : destroyedBlocks) {
            if (placedBlocks.contains(blockData.getLocation())) {
                placedBlocks.remove(blockData.getLocation());
            }
        }

        for (Location l : placedBlocks) {
            l.getBlock().setType(Material.AIR);
        }

        for (BlockData blockData : destroyedBlocks) {
            blockData.regen();
        }
    }

    public static List<BlockData> getDestroyedBlocks() {
        return destroyedBlocks;
    }

    public static List<Location> getPlacedBlocks() {
        return placedBlocks;
    }

    public static List<Location> getRemove() {
        return remove;
    }
}
