package com.falcon.luckyisland.block;

import org.bukkit.Location;

public class BlockData {

    private int id;
    private byte data;
    private Location location;

    public BlockData(int id, byte data, Location location) {
        this.id=id;
        this.data=data;
        this.location=location;
    }

    public int getId() {
        return id;
    }

    public byte getData() {
        return data;
    }

    public Location getLocation() {
        return location;
    }

    public void regen() {
        location.getBlock().setTypeIdAndData(id,data,true);
    }
}
