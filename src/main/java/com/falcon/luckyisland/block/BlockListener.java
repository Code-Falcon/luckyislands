package com.falcon.luckyisland.block;

import com.falcon.luckyisland.LuckyIsland;
import com.falcon.luckyisland.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class BlockListener implements Listener {

    @EventHandler
    public void explode(EntityExplodeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void blockPlace(BlockPlaceEvent event) {
        if (!event.isCancelled()) {
            BlockAPI.getPlacedBlocks().add(event.getBlock().getLocation());
        }
    }

    @EventHandler
    public void blockListener(BlockBreakEvent event) {

        if (LuckyIsland.getPlugin().getGameManager().getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }

        Player player = event.getPlayer();

        if (LuckyIsland.getPlugin().getGameManager().getPlayers().contains(player.getUniqueId())) {
            BlockAPI.getDestroyedBlocks().add(new BlockData(event.getBlock().getTypeId(),event.getBlock().getData(),event.getBlock().getLocation()));
            if (event.getBlock().getType() == Material.SPONGE) {
                event.setDropItems(false);

                int i = new Random().nextInt(LuckyBlock.values().length);
                LuckyBlock block = null;

                for (LuckyBlock luckyBlock : LuckyBlock.values()) {
                    if (luckyBlock.getId() == i) {
                        block=luckyBlock;
                    }
                }

                if (block == LuckyBlock.MR_CREEPER) {
                    Creeper creeper = Bukkit.getWorld(player.getWorld().getName()).spawn(event.getBlock().getLocation(),Creeper.class);
                    creeper.setCustomName("§c§lMr. Creeper");
                    creeper.setTarget(player);
                    player.sendMessage("§cMr. Creeper is coming for you");
                } else if (block == LuckyBlock.HEALTH) {
                    player.setMaxHealth(player.getMaxHealth()+2);
                    Bukkit.getWorld(player.getWorld().getName()).playEffect(event.getBlock().getLocation(), Effect.HEART,1);
                } else if (block == LuckyBlock.TOOLS) {
                    event.getBlock().getWorld().dropItem(event.getBlock().getLocation(),new ItemStack(Material.IRON_PICKAXE));
                } else if (block == LuckyBlock.BLOCKS) {
                    event.getBlock().getWorld().dropItem(event.getBlock().getLocation(),new ItemStack(Material.WOOD,16));
                }
            }
        } else {
            event.setCancelled(true);
            player.sendMessage("§cYou cannot break blocks as a spectator!");
        }

    }
}
