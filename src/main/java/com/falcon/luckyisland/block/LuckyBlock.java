package com.falcon.luckyisland.block;

public enum LuckyBlock {

    HEALTH(0),MR_CREEPER(1),TOOLS(2),BLOCKS(3);

    private int id;

    LuckyBlock(int id) {
        this.id=id;
    }

    public int getId() {
        return id;
    }
}
