package com.falcon.luckyisland.game;

import com.falcon.luckyisland.LuckyIsland;
import com.falcon.luckyisland.threads.IngameThread;
import com.falcon.luckyisland.util.ConfigAPI;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import javax.swing.plaf.LabelUI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GameManager {

    private List<UUID> players;
    private GameState gameState;

    public GameManager() {
        gameState=GameState.LOBBY;
        players=new ArrayList<>();
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void addSpectator(Player player) {
        player.sendMessage("§aYou are now spectating");

        for (Player pl : Bukkit.getOnlinePlayers()) {
            if (pl != player) {
                pl.hidePlayer(player);
            }
        }

        player.setAllowFlight(true);

        ItemStack teleport = new ItemStack(Material.COMPASS);
        ItemMeta itemMeta = teleport.getItemMeta();
        itemMeta.setDisplayName("§aPlayer Teleporter");
        teleport.setItemMeta(itemMeta);

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);

        player.getInventory().setItem(0,teleport);

    }

    public void sendScoreboard(String args) {

        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("sidebar","dummy");

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§6LuckyIslands");

        if (getGameState() == GameState.LOBBY) {
            Score players = objective.getScore("§aPlayers: §e" + Bukkit.getOnlinePlayers().size());
            players.setScore(10);
            Score blank = objective.getScore("  ");
            blank.setScore(9);
            Score map = objective.getScore("§aMap: §e" + ConfigAPI.loadString("setup.name"));
            map.setScore(8);
            Score blank1 = objective.getScore(" ");
            blank1.setScore(7);
            Score time = objective.getScore("§aTime Left: §e" + args);
            time.setScore(6);
        } else if (getGameState() == GameState.INGAME) {
            Score players = objective.getScore("§aPlayers: §e" + getPlayers().size());
            players.setScore(10);
            Score blank = objective.getScore("  ");
            blank.setScore(9);
            Score map = objective.getScore("§aMap: §e" + ConfigAPI.loadString("setup.name"));
            map.setScore(8);
        } else if (getGameState() == GameState.OVER) {

        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            player.setScoreboard(scoreboard);
        }

    }

    public void prepIngame() {
        new IngameThread(LuckyIsland.getPlugin());
    }

    public void win(Player player) {
        setGameState(GameState.OVER);
        Bukkit.broadcastMessage("§6---------------");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§aWinner: §7" + player.getName());
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§6---------------");

        for (Player pl : Bukkit.getOnlinePlayers()) {
            pl.teleport(ConfigAPI.loadLocation("lobby"));
        }

        Bukkit.broadcastMessage("§6Game restarting... §aDo not leave, the next round will be starting soon!");

        new BukkitRunnable() {

            public void run() {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.teleport(ConfigAPI.loadLocation("lobby"));

                    pl.getInventory().clear();
                    pl.getInventory().setArmorContents(null);
                    pl.setMaxHealth(20);
                }

                Bukkit.reload();
            }

        }.runTaskLater(LuckyIsland.getPlugin(),200L);
    }

    public void startGame() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            getPlayers().add(player.getUniqueId());
        }

        int i = 0;

        setGameState(GameState.INGAME);

        for (Player player : Bukkit.getOnlinePlayers()) {
            i=i+1;

            player.teleport(ConfigAPI.loadLocation("spawns.spawn"+i));
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);

            player.setLevel(0);
            player.setGameMode(GameMode.SURVIVAL);
            player.setPlayerListName("§a" + player.getName());
            player.setMaxHealth(20);
        }

        prepIngame();
    }

    public List<UUID> getPlayers() {
        return players;
    }
}
