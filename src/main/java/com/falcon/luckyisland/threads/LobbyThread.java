package com.falcon.luckyisland.threads;

import com.falcon.luckyisland.LuckyIsland;
import com.falcon.luckyisland.game.GameManager;
import com.falcon.luckyisland.game.GameState;
import com.falcon.luckyisland.util.ConfigAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class LobbyThread extends BukkitRunnable {

    private int id;

    private int time;

    public LobbyThread(Plugin plugin) {
        time=60;
        this.id= Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,this,20L,20L);
    }

    @Override
    public void run() {
        if (LuckyIsland.getPlugin().getGameManager().getGameState() != GameState.LOBBY) {
            Bukkit.getScheduler().cancelTask(id);
            return;
        }

        time=time-1;

        LuckyIsland.getPlugin().getGameManager().sendScoreboard(time + "");

        for (Player player : Bukkit.getOnlinePlayers()) {
            player.setLevel(time);
        }

        if (time == 50 || time == 40 || time == 30 || time == 20) {
            Bukkit.broadcastMessage("§aLuckyIslands will begin in §e" + time + " seconds");
        }

        if (time == 10) {
            if (Bukkit.getOnlinePlayers().size() >= ConfigAPI.loadInt("setup.start")) {
                Bukkit.broadcastMessage("§6There are enough players to start the game");
            } else {
                Bukkit.broadcastMessage("§cThere are not enough players to start the game");
                time=60;
            }
        }

        if (time == 0) {
            LuckyIsland.getPlugin().getGameManager().startGame();
        }
    }
}
