package com.falcon.luckyisland.threads;

import com.falcon.luckyisland.LuckyIsland;
import com.falcon.luckyisland.game.GameManager;
import com.falcon.luckyisland.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class IngameThread extends BukkitRunnable {

    private int id;

    public IngameThread(Plugin plugin) {
        this.id= Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,this,20L,20L);
    }

    @Override
    public void run() {
        if (LuckyIsland.getPlugin().getGameManager().getGameState() != GameState.INGAME) {
            Bukkit.getScheduler().cancelTask(id);
            return;
        }

        LuckyIsland.getPlugin().getGameManager().sendScoreboard("");

        if (LuckyIsland.getPlugin().getGameManager().getPlayers().size() == 1) {
            for (UUID uuid : LuckyIsland.getPlugin().getGameManager().getPlayers()) {
                LuckyIsland.getPlugin().getGameManager().win(Bukkit.getPlayer(uuid));
            }
        }
    }
}
