package com.falcon.luckyisland.listeners;

import com.falcon.luckyisland.LuckyIsland;
import com.falcon.luckyisland.game.GameState;
import com.falcon.luckyisland.util.ConfigAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (LuckyIsland.getPlugin().getGameManager().getGameState() == GameState.LOBBY) {
            event.setJoinMessage("§a" + event.getPlayer().getDisplayName() + "§a has joined");
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);

            player.teleport(ConfigAPI.loadLocation("lobby"));
        }
    }
}
