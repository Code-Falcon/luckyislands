package com.falcon.luckyisland.listeners;

import com.falcon.luckyisland.LuckyIsland;
import com.falcon.luckyisland.game.GameState;
import com.falcon.luckyisland.util.ConfigAPI;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerDeath implements Listener {

    @EventHandler
    public void damage(EntityDamageEvent event) {
        if (LuckyIsland.getPlugin().getGameManager().getGameState() != GameState.INGAME) {
            event.setCancelled(true);
        }

        if (event.getEntity().getType() == EntityType.PLAYER) {
            Player player = (Player) event.getEntity();

            if (!LuckyIsland.getPlugin().getGameManager().getPlayers().contains(player.getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void entityDamage(EntityDamageByEntityEvent event) {
        if (event.getEntity().getType() == EntityType.PLAYER) {
            if (event.getDamager().getType() == EntityType.PLAYER) {
                Player damager = (Player) event.getDamager();

                if (!LuckyIsland.getPlugin().getGameManager().getPlayers().contains(damager.getUniqueId())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void playerDeath(PlayerDeathEvent event) {
        String message = "";

        if (event.getEntity().getKiller() == null) {
            message="§edied!";
        } else {
            message="§ewas killed by §c" + event.getEntity().getKiller().getName();
        }

        event.setDeathMessage("§a" + event.getEntity().getName() + " " + message);

        if (LuckyIsland.getPlugin().getGameManager().getPlayers().contains(event.getEntity().getUniqueId())) {
            LuckyIsland.getPlugin().getGameManager().getPlayers().remove(event.getEntity().getUniqueId());
        }
    }

    @EventHandler
    public void playerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        player.teleport(ConfigAPI.loadLocation("spawns.spawn1"));
        LuckyIsland.getPlugin().getGameManager().addSpectator(player);
    }
}
