package com.falcon.luckyisland.util;

import com.falcon.luckyisland.LuckyIsland;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import javax.swing.plaf.LabelUI;

public abstract class ConfigAPI {

    public static void setLocation(Location location, String s) {

        LuckyIsland.getPlugin().getConfig().set(s+".world",location.getWorld().getName());
        LuckyIsland.getPlugin().getConfig().set(s+".x",location.getX());
        LuckyIsland.getPlugin().getConfig().set(s+".y",location.getY());
        LuckyIsland.getPlugin().getConfig().set(s+".z",location.getZ());
        LuckyIsland.getPlugin().saveConfig();
    }

    public static void setString(String s,String s1) {
        LuckyIsland.getPlugin().getConfig().set(s,s1);
        LuckyIsland.getPlugin().saveConfig();
    }

    public static void setInt(String s,int s1) {
        LuckyIsland.getPlugin().getConfig().set(s,s1);
        LuckyIsland.getPlugin().saveConfig();
    }

    public static int loadInt(String s) {
        return LuckyIsland.getPlugin().getConfig().getInt(s);
    }

    public static String loadString(String s) {
        return LuckyIsland.getPlugin().getConfig().getString(s);
    }

    public static Location loadLocation(String s) {

        Location l = null;

        double x = LuckyIsland.getPlugin().getConfig().getDouble(s+".x");
        double y = LuckyIsland.getPlugin().getConfig().getDouble(s+".y");
        double z = LuckyIsland.getPlugin().getConfig().getDouble(s+".z");
        String world = LuckyIsland.getPlugin().getConfig().getString(s+".world");

        l=new Location(Bukkit.getWorld(world),x,y,z);

        return l;
    }
}
