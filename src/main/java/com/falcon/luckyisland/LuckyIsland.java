package com.falcon.luckyisland;

import com.falcon.luckyisland.block.BlockAPI;
import com.falcon.luckyisland.block.BlockListener;
import com.falcon.luckyisland.command.LuckyIslandCommand;
import com.falcon.luckyisland.game.GameManager;
import com.falcon.luckyisland.listeners.PlayerDeath;
import com.falcon.luckyisland.listeners.PlayerJoin;
import com.falcon.luckyisland.threads.LobbyThread;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class LuckyIsland extends JavaPlugin {

    private static LuckyIsland plugin;

    private GameManager gameManager;
    private PluginManager pluginManager = Bukkit.getPluginManager();

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        getLogger().info("LuckyIslands 1.0.0 has been enabled");

        plugin=this;

        BlockAPI.init();

        gameManager=new GameManager();

        getCommand("luckyisland").setExecutor(new LuckyIslandCommand());

        pluginManager.registerEvents(new PlayerJoin(),this);
        pluginManager.registerEvents(new BlockListener(),this);
        pluginManager.registerEvents(new PlayerDeath(),this);

        new LobbyThread(this);
    }

    @Override
    public void onDisable() {
        BlockAPI.regen();
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public static LuckyIsland getPlugin() {
        return plugin;
    }
}
