package com.falcon.luckyisland.command;

import com.falcon.luckyisland.LuckyIsland;
import com.falcon.luckyisland.block.BlockAPI;
import com.falcon.luckyisland.util.ConfigAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LuckyIslandCommand implements CommandExecutor {

    public void sendUsage(Player player) {
        player.sendMessage("§6------§e[§aLuckyIsland§e]§6------");
        player.sendMessage("§a/luckyisland ");

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {

        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("luckyisland")) {
            if (!player.hasPermission("luckyisland.admin")) {
                player.sendMessage("§aLuckyIslands version 1.0.0");
                return true;
            }
            if (args.length == 0) {
                sendUsage(player);
                return true;
            }

            if (args[0].equalsIgnoreCase("setlobby")) {
                ConfigAPI.setLocation(player.getLocation(),"lobby");
                player.sendMessage("§aSet lobby location");
            } else if (args[0].equalsIgnoreCase("setspawn")) {
                if (args.length == 1) {
                    sendUsage(player);
                    return true;
                }

                int i = Integer.parseInt(args[1]);

                if (i > 8) {
                    player.sendMessage("§cThe maximum number of players supported is 8!");
                    return true;
                }

                if (i == 0 || i < 0) {
                    player.sendMessage("§cInvalid number!");
                    return true;
                }

                ConfigAPI.setLocation(player.getLocation(),"spawns.spawn" + i);
                player.sendMessage("§aSet spawn location for " + i);
            } else if (args[0].equalsIgnoreCase("setname")) {
                if (args.length == 1) {
                    sendUsage(player);
                    return true;
                }

                String name = args[1];

                ConfigAPI.setString("setup.name",name);
                player.sendMessage("§aSet map name to §e" + name);
            } else if (args[0].equalsIgnoreCase("start")) {
                LuckyIsland.getPlugin().getGameManager().startGame();
            } else if (args[0].equalsIgnoreCase("regen")) {
                BlockAPI.regen();
            } else if (args[0].equalsIgnoreCase("forcewin")) {
                LuckyIsland.getPlugin().getGameManager().win(player);
            }
        }

        return false;
    }
}
